export enum WsMessageTypes {
  update = 'UPDATE',
  jobFinished = 'JOB_FINISHED',
  user = 'USER',
  newReleases = 'NEW_RELEASES',
  recommendations = 'RECOMMENDATIONS',
  genres = 'GENRES',
  accepted = 'ACCEPTED',
  error = 'ERROR'
}

export enum JobFinishedStatus {
  success = 'SUCCESS',
  failure = 'FAILURE'
}
